# Flask App explictly Vulnerbale with Secrets

For GitLab Team Memebers to demo Secret Detection to our customers.

## Runbook

1. Fork this project into an Ultimate group for yourself.
2. Remove the [Forking relationship](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork).
3. Activate Secret Detection via the [UI through an automatically configured merge request](https://docs.gitlab.com/ee/user/application_security/secret_detection/#use-an-automatically-configured-merge-request).
4. Show after the Pipeline has run the found secrets in the security Tab of the Pipeline.
5. In the Pipeline Secuirty Tab select the 4 Secrets found in the Readme.md and dismiss those with `Won't fix, Accpet risk`.
6. Switch to the Vulnerability report, show the none dismissed and how you can view the dismissed vulns.
7. Show one vulnerability (GCP OAuth client secret) in the popup modal.
8. Change its status to Confirmed and Create an issue out of this.
9. Go back to the vulnerability report and select all findings with statusm "Needs Triage".
10. Set their status to confirmed.
11. Explain that there are some hidden Secrets, which were deleted with newer commits.
12. Open th epipeline Editor and Copy Paste the code from the [CI Yaml File for Secret detection](#ci-yaml-file-for-secret-detection) into the file.
13. Commit the changes to main and a pipeline on the default brnach with the new settings (Git History) and you can show off that now more Secrets were found (Also in the Job Log).
14. In the Secuity Tab you can play with the Hide dismissed button.
15. Switch to the Vulnerability Report to ientify the new findings, they have still the status "Needs Triage".
16. **Custom Rules:**
17. Show in the file [`app.py` in line 73](/app.py#L73) the `headers = {"Authorization": "Bearer MYREALLYLONGTOKENIGOT"}` and with it, that this Api key has not been detected.
18. Open the WebIDE in the Repo.
19. Add the file  `.gitlab/secret-detection-ruleset.toml` with the content [from underneath](#gitlab-secret-detection-rulesettoml) and the extended config with file `.gitlab/extended-gitleaks-config.toml` including content [from here](#gitlab-extended-gitleaks-configtoml). (Used concept are Allowlist and extend, used RegEx [found here](https://regex101.com/r/PGscpO/1)).
20. The Push to main executes a pipeline on the default branch with the new findings, which you can show in the Security Tab of the Pipeline and the Vulnerability report.
21. Set the new found Vulnerabilities in the Report to Status COnfirmed.
22. _Optional:_ **Security Policies (Scan Reuslt Policy):**
23. _Optional:_ Go to in your repo to `security/policies`.
24. _Optional:_ Click `New Policy`.
25. _Optional:_ In `Scan result policy` click `Select Policy`.
26. _Optional:_ Give Name, for example `No Secrets Here`.
27. _Optional:_ Select: When `Security Scan` `Secret Detection` runs against the `main` and find(s) `Any` vulnerabilities that match all of the following criteria:
28. _Optional:_ Click Add new criteria -> New Status.
29. _Optional:_ Status is: `New` `All vulnerability states`.
30. _Optional:_ Click Add new criteria -> New Severity.
31. _Optional:_ Select Severity level -> Select All.
32. _Optional:_ Got to the Actions part.
33. _Optional:_ Require 1 approval from `Groups`: `Enter a Group Name` for example `timtams`.
34. _Optional:_ Click `Create with a Merge Request`.
35. _Optional:_ `Merge` the Merge request.
36. _Optional:_ Show off the newly create [Security policy project](https://docs.gitlab.com/ee/user/application_security/policies/#security-policy-project).
37. _Optional:_ Show the `.gitlab/security-policies/policy.yml` in that project as rules as Code.
38. _Optional:_ Switch back to your original project.
39. **Show the addition of a secret in a Merge request.**
40. Open an issue in your Project, call it `Adding a Secret`.
41. Add in the description this GitLab PAT to show of the warning dialogue: `glpat-JUST20LETTERSANDNUMB`.
42. Open a Merge Request out of this issue.
43. Open the WebIDE in this Merge Request.
44. Open any file of the repo in the WebIDE.
45. Add this content to the file: `Demo Token: glpat-JUST20LETTERSANDNUMB`.
46. Push the change via the WebIDE to your repo and the MR branch.
47. Open the Merge Request again and show the pipeline plus after it has run the security finding.
48. _Optional:_ If you have security Policies setup, show that it needs aproval.
49. Open again the WebIDE with the file where you had added the Secret.
50. Show how you can [Ignore Secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#ignore-secrets).
51. Add behind the Secret in your code: `#gitleaks:allow`.
52. Commit the Code Change.
53. Show that the Secret is not found anymore and you can Merge.
54. Megre the MR.
55. **Activate Push Rules:**
56. Go in the NavBar to `Settings`, there `Repository Settings` and Expand `Push rules`.
57. Activate `Prevent pushing secret files` and click `Save Push Rules`.
58. Back in the repository open the WebIDE.
59. Create a new file with the name 'id_rsa'.
60. Copy in the content from below [Demo RSA private Key](#demo-rsa-private-key).
61. Commit and push the changes to the main brnach.
62. Show the Error message which comes up.

### gitlab-secret-detection-ruleset.toml

* Add file gitlab/secret-detection-ruleset.toml

```bash
.gitlab/secret-detection-ruleset.toml
```

* Add content to file gitlab/secret-detection-ruleset.toml

```yaml
# .gitlab/secret-detection-ruleset.toml
[secrets]
  description = 'secrets custom rules configuration'

  [[secrets.passthrough]]
    type  = "file"
    target = "gitleaks.toml"
    value = ".gitlab/extended-gitleaks-config.toml"
```

### gitlab-extended-gitleaks-config.toml

* Add file gitlab/extended-gitleaks-config.toml

```bash
.gitlab/extended-gitleaks-config.toml
```

* Add content to file gitlab/extended-gitleaks-config.toml

```yaml
# extended-gitleaks-config.toml
title = "extension of gitlab's default gitleaks config"

[extend]
# Extends default packaged path
path = "/gitleaks.toml"

[allowlist]
  description = "allow list of test tokens to ignore in detection"
  regexTarget = "match"
  regexes = ['''glpat-1234567890abcdefghij''',]

# add Test RegEx to the regex table
[[rules]]

# Unique identifier for this rule
id = "test-rule-1"

# Short human readable description of the rule.
description = "Test for Raw Custom Rulesets"

# Golang regular expression used to detect secrets. Note Golang's regex engine
# does not support lookaheads.
regex = '''Custom Raw Ruleset T[est]{3'''

[[rules]]
id = "bearer-auth-token"
description = "Bearer Tokens"
regex = ''':\s*['" ](?i)bearer\S* \S+'''
```

### Demo RSA private Key

```bash
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAwFR40KI1SzBR0njomkDDEeS0c4r6+7HoUlzqE24hcnP0eJW1
xa2i1dBMkhm56bhWIIq6w6F9Fg8bd5Tsr72OYsiDpoeJ6KV3RVFDv6Ighja+/WdP
4DroQfq0YVo2XoBSPb5uhv46/pNUa5Y+rwVh+SPQlH8sxE7LZODoy8FD2+GskSUs
jk9Y3Cca5/cv7wafh/i3/De5tE5DG1pLlNwbdAG3xCgcgS2806XCMgA5mXt9HXqn
vyuoyyG2gW6sdHb1pwf8rlgoRLbPqwbLpOi245f5XqLmmwBu4Nxc3scseDXfuHDp
Nky1cOzIBePVxktM2DlAAPQ+7Em23TJr0pmrQQIDAQABAoIBAQCnsX9dufDpzAmr
kAyPYmQzV8wW6lkH2AkOt0DJDD9RgdToxvAkmc7eyq3YvWGibT17RjqtlEJyV13F
mC3+1TIu41IWgxs1pAAoikCd+AiPvXAtlkTI59PWo3dfYr8BCrWqbD4GqehaS69R
10B0bicMibO1pmUsDN++53NTJQG71rZp8o3b39Pse3ON/fQLpflPdaBOGkcyXVu5
UB4UDpO8PHtGU3IsxdpM1AVLpari/cYTksgTVczYtYDcTT4Ud2kuB4Q9OmwDTT2K
uecn7S4FR1FS58ILHhC67gWO/A6DnXTUqwiD8/pDY8QJcRQviWoEBQyhMycZSH+O
enzJtCPZAoGBAPPgVlFRMM99r2zSqT0btIG986Pyh3q8dz8ZvfdwUSIbRgqPAG47
ZukRqIVl2d7S9yVtRnFnDteNokG4d5Mk1a3syn87iFKE6wBpr4htWSlOlntVakou
iQq1ngFcA6ABAwtv2eEa1BPaWMuW7q3yWdOYrC36vX1A03llyp4xFNzfAoGBAMnk
IuT1ZRJZpAqd9SveEqW+aYpUrFetYz3JhVuNT49vJoBiq1RrWvgfuEC7gayEDDfb
Gep11XnXPkXspN6415kdarOgiE9CQlADNG9fk0D61O5ONZZTrqGWEBythfoV2xSk
xb6YDPuxs0S6MylQAc9ZRVUpVGLnytHsKjAMVlvfAoGASxFZ4Iv6V1QbxIaPu5Sk
mm8q6ONFmp0ao5y74cd74eC9TZC5FDVKtyFNW0p/ptwPYUDitxN++RDKyioK/IsR
DwldR46+po/temINuxPVpyZeobYoEo+CdX50FX0KTJ0jH8kdKvJEJ5xFSt25uGdq
CPzsuvZ8j2p97ddMaCc5gccCgYAdUI7wh+FBJNr43661y+0RO/C/MURFBtweIKDI
hmBDB3Sjt7AA9gWjeZebbp6JmjLb+Wht7uYsZuCX7qCR5m0Hwom3w1uHhqtySsTW
Vx5elQ1N/PUy+rukotF8GIYXpgzFlpdP8WwRL+BD3nWHTiK1JNU4ZGPoaJe+m3gU
ufXgKQKBgFdbqJfnSrXyrJcDH9nzMwnR6wyFc0RnILie1lrcQZiru9s/Tlv71GJ/
nMEybyOailEeSlBKYy04uVJGPVO4bnzIiGmJdgZlxxjEwHpx/6bkBnh4YVbRAHgc
al8MbhHvfVaRRdRW8eRVuoeHfPge8fRr7UtloYbOEpZh9nTxMUHj
-----END RSA PRIVATE KEY-----
```

## Resources

* [Docu Push rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#prevent-pushing-secrets-to-the-repository)
* [Files deny list for push rules](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/gitlab/checks/files_denylist.yml)
* [Secuirty Policies in docs](https://docs.gitlab.com/ee/user/application_security/policies/#security-policy-project)
* [Secret Dteection in docs](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
* [Saved regex](https://regex101.com/r/PGscpO/1)

### RegEx Examplesa

```python
regex = '`:\s*['" ](?i)bearer\S* \S+`gm'
headers = {"Authorization": "Bearer MYREALLYLONGTOKENIGOT"}
metaData= {'authorization' : "bearer jdewRVKNL3AQ52AWSZwQ9WddDbOvHtQuW2O",
WWW-Authenticate: Bearer realm="Keksfabrik"
Authorization: Bearer S0VLU0UhIExFQ0tFUiEK
curl -H "Authorization: bearer MYREALLYLONGTOKENIhad" http://www.example.com
```

### CI Yaml File for Secret detection

```yaml
# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
include:
- template: Security/Secret-Detection.gitlab-ci.yml

# https://docs.gitlab.com/ee/user/application_security/secret_detection/#configure-scan-settings
secret_detection:
  variables:
    SECRET_DETECTION_EXCLUDED_PATHS: "test, README.md" # Exclude the content of the test folder and README.md
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:                              # Override SECRET_DETECTION_HISTORIC_SCAN
        SECRET_DETECTION_HISTORIC_SCAN: "true"  # at the job level.
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        SECRET_DETECTION_HISTORIC_SCAN: "false" # On not default branch ignore History

# Better Logs for your Job Logs test
variables:
  SECURE_LOG_LEVEL: debug
```

## _Optional_ Commands for local installation and prep

* Git Clone this repo
* virtual enviroment `python3 -m venv .`
* Install reqs `python3 -m  pip install -r requirements.txt`
* run app `python3 -m flask run`

## _Optional_ Artifial vulns

### Werkzeug

* [werkzeug@0.15.1 vulnerabilities](https://security.snyk.io/package/pip/werkzeug/0.15.1) - version 0.15.1

### Flask

* [flask@0.12.2 vulnerabilities](https://security.snyk.io/package/pip/flask/0.12.2) - 0.12.2

### Image

* [python:3.8-slim-buster](https://hub.docker.com/layers/library/python/3.8-slim-buster/images/sha256-4bc25e1810c14af78ca0235ff6f63740113540fd49f8dc5bc6c7b456572b5806?context=explore)

---

## ToDo

* [x] Find GHB patter and replace the not working keys with working ones, then setup the demo again
* [x] Check custom ruleset on regex: `regex = ''':\s*['" ](?i)bearer\S* \S+'''`
* [x] Check Security Policy as this does not work at the moment
* [ ] Refresh Whole Porject and hide secrets agan
* [ ] Add GitLeaks Local: https://gitlab.com/gitlab-com/gl-security/security-research/gitleaks-endpoint-installer
* [ ] Add Secuirty Execution scheduled Pipeline with customer Variables: https://gitlab.com/groups/gitlab-org/-/epics/9566
